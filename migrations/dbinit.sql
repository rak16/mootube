CREATE TABLE videos (
    id SERIAL PRIMARY KEY,
    video_id TEXT NOT NULL,
    title TEXT NOT NULL,
    description TEXT, 
    thumbnail TEXT NOT NULL,
    published_at TIMESTAMP NOT NULL
);
FROM node:10

WORKDIR /app

COPY ./package.json .
COPY ./package-lock.json .
COPY ./wait-for-it.sh .
RUN chmod +x ./wait-for-it.sh

RUN npm install

COPY . .

EXPOSE 5000

CMD ./wait-for-it.sh postgres:5432 -t 60 -- npm start
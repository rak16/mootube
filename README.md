# 🏗️ Setup

1. Checkout to `develop`.
2. Make sure you have docker and docker-compose installed.
    - Run `which docker`. If this does not return a path, refer to: https://docs.docker.com/get-docker/
    - Run `which docker-compose`. If this does not return a path, refer to: https://docs.docker.com/compose/install/
3. Run `docker-compose up --build -d` in the root directory to run the server.

> Note: If you have an instance of postgres already running, you might face a port conflict. Try changing the port to something else in the `docker-compose.yml` file.

# 🔌 API Reference

1. Get all videos

    > GET localhost:5000/api/v1/video?limit=10&offset=2

   ```
   [
        {
            "id": 3,
            "video_id": "IRQqkQsci6I",
            "title": "MAN CITY 5-0 EVERTON | PREMIER LEAGUE HIGHLIGHTS",
            "description": "Everton suffered a heavy defeat on the final day of the 2020/21 season, with Premier League champions Manchester City running out 5-0 winners.",
            "thumbnail": "https://i.ytimg.com/vi/IRQqkQsci6I/default.jpg",
            "published_at": "2021-05-23T21:00:12.000Z"
        },
        {
            "id": 4,
            "video_id": "1eIP1SgudRs",
            "title": "Aston Villa 2-1 Chelsea | Chelsea qualify for Champions League despite defeat | Highlights",
            "description": "The sweetest of defeats for Chelsea that creeps them into next season's Champions League despite losing 2-1 to Aston Villa on the final day of the Premier ...",
            "thumbnail": "https://i.ytimg.com/vi/1eIP1SgudRs/default.jpg",
            "published_at": "2021-05-23T21:00:01.000Z"
        },
        {
            "id": 5,
            "video_id": "X_oNTkBcgLQ",
            "title": "JAYLEN WADDLE 2018-20 Alabama College Football Career Highlights | Jake Reacts!",
            "description": "THANKS FOR WATCHING AND MUCH LOVE! BECOME A PATREON & SUPPORT THE CHANNEL: https://www.patreon.com/jacobmcdonald27 PS: Sorry about ...",
            "thumbnail": "https://i.ytimg.com/vi/X_oNTkBcgLQ/default.jpg",
            "published_at": "2021-05-23T20:11:55.000Z"
        },
        {
            "id": 6,
            "video_id": "KRZaLvTY_MU",
            "title": "Chelsea &amp; Liverpool MAKE IT | Aston Villa 2-1 Chelsea | Premier League Season Review",
            "description": "We are Football Fan Media for real football fans, bringing you big match previews, post-match reaction, reviews, tactical analysis, player debate & fan media ...",
            "thumbnail": "https://i.ytimg.com/vi/KRZaLvTY_MU/default.jpg",
            "published_at": "2021-05-23T20:05:16.000Z"
        },
        {
            "id": 7,
            "video_id": "6H7E9QEFa60",
            "title": "Mane Goals Secure CL Football! | Liverpool 2-0 Palace | Liverpool Fan Goal Reactions",
            "description": "Mane Goals Secure CL Football! | Liverpool 2-0 Palace | Liverpool Fan Goal Reactions Re-live Liverpool's 2-0 win over Crystal Palace as we head to Hotel ...",
            "thumbnail": "https://i.ytimg.com/vi/6H7E9QEFa60/default.jpg",
            "published_at": "2021-05-23T19:59:20.000Z"
        },
        {
            "id": 8,
            "video_id": "b4UENmU5eDE",
            "title": "wrong heads top superheroes vlad &amp; niki football candy - meme coffin dance cover astronomia sang",
            "description": "wrong heads top superheroes vlad & niki football candy - meme coffin dance cover astronomia sang captain america and hulk save the world iron man green ...",
            "thumbnail": "https://i.ytimg.com/vi/b4UENmU5eDE/default_live.jpg",
            "published_at": "2021-05-23T18:47:18.000Z"
        },
        {
            "id": 9,
            "video_id": "ZulsmA8kWY8",
            "title": "Highlights and All Goals of today football matches/Premier League today",
            "description": "football highlights football highlights today today football match highlights highlight football today today match highlights match highlights today highlight football ...",
            "thumbnail": "https://i.ytimg.com/vi/ZulsmA8kWY8/default.jpg",
            "published_at": "2021-05-23T18:41:49.000Z"
        },
        {
            "id": 10,
            "video_id": "I_fcWKE4UJs",
            "title": "Pep Guardiola breaks down into tears speaking about Sergio Aguero leaving Manchester City",
            "description": "SUBSCRIBE ▻ http://bit.ly/SSFootballSub PREMIER LEAGUE HIGHLIGHTS ▻ http://bit.ly/SkySportsPLHighlights Pep Guardiola speaks exclusively to Sky ...",
            "thumbnail": "https://i.ytimg.com/vi/I_fcWKE4UJs/default.jpg",
            "published_at": "2021-05-23T18:17:54.000Z"
        }
    ]
    ```

2. Search for videos
   
    > GET localhost:5000/api/v1/video/search?limit=10&offset=2&q=League
   
    ```
    [
        {
            "id": 6,
            "video_id": "KRZaLvTY_MU",
            "title": "Chelsea &amp; Liverpool MAKE IT | Aston Villa 2-1 Chelsea | Premier League Season Review",
            "description": "We are Football Fan Media for real football fans, bringing you big match previews, post-match reaction, reviews, tactical analysis, player debate & fan media ...",
            "thumbnail": "https://i.ytimg.com/vi/KRZaLvTY_MU/default.jpg",
            "published_at": "2021-05-23T20:05:16.000Z"
        },
        {
            "id": 9,
            "video_id": "ZulsmA8kWY8",
            "title": "Highlights and All Goals of today football matches/Premier League today",
            "description": "football highlights football highlights today today football match highlights highlight football today today match highlights match highlights today highlight football ...",
            "thumbnail": "https://i.ytimg.com/vi/ZulsmA8kWY8/default.jpg",
            "published_at": "2021-05-23T18:41:49.000Z"
        },
        {
            "id": 12,
            "video_id": "lanYWuGcY6A",
            "title": "Manchester City lift the 2020/21 Premier League trophy! 🏆",
            "description": "SUBSCRIBE ▻ http://bit.ly/SSFootballSub PREMIER LEAGUE HIGHLIGHTS ▻ http://bit.ly/SkySportsPLHighlights Watch Sky Sports' coverage of Manchester ...",
            "thumbnail": "https://i.ytimg.com/vi/lanYWuGcY6A/default.jpg",
            "published_at": "2021-05-23T18:00:25.000Z"
        },
        {
            "id": 13,
            "video_id": "NLMQufxODuU",
            "title": "LIVERPOOL vs CRYSTAL PALACE - LIVE STREAMING - Premier League - Football Match",
            "description": "Liverpool vs Crystal Palace live stream. Join the live football full match chat. Liverpool vs Crystal Palace Premier League live at Liverpool and it will be streamed ...",
            "thumbnail": "https://i.ytimg.com/vi/NLMQufxODuU/default.jpg",
            "published_at": "2021-05-23T17:56:41.000Z"
        },
        {
            "id": 18,
            "video_id": "TVuijlslyLQ",
            "title": "Matchday Live: Aston Villa v Chelsea | Pre-Match | Premier League Matchday",
            "description": "Welcome to the official home of Chelsea Football Club on YouTube. It's the only channel where you'll get an authentic look at life at this great club. Every week ...",
            "thumbnail": "https://i.ytimg.com/vi/TVuijlslyLQ/default.jpg",
            "published_at": "2021-05-23T15:17:52.000Z"
        }
    ]
    ```

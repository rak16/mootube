'use strict';

const db = require('../services/db.service');
const config = require('../config/config');

const getAllVideos = async (req, res, next) => {
    const limit = req.query.limit;
    const offset = req.query.offset;

    if (typeof limit == 'undefined' || typeof offset == 'undefined') {
        return res.status(422).send('Mandtory fields: limit, offset');
    }

    const videos = await db.getVideos(limit, offset);
    return res.status(200).json(videos);
}

const getVideosBySearchKey = async (req, res, next) => {
    const key = req.query.q;
    const limit = req.query.limit;
    const offset = req.query.offset;

    if (typeof limit == 'undefined' || typeof offset == 'undefined') {
        return res.status(422).send('Mandtory fields: limit, offset');
    }

    const videos = await db.getVideos(limit, offset, key);
    return res.status(200).json(videos);
}

module.exports = {
    getAllVideos,
    getVideosBySearchKey,
};
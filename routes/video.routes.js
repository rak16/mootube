'use strict';

const asyncHandler = require('express-async-handler');

const {
    getAllVideos,
    getVideosBySearchKey
} = require('../controllers/video.controller');

const urlPrefix = '/api/v1';

module.exports = app => {
    app.get(`${urlPrefix}/video`, asyncHandler(getAllVideos));
    app.get(`${urlPrefix}/video/search`, asyncHandler(getVideosBySearchKey));
};
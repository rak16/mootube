module.exports = {
    CLIENT_URL: 'http://localhost:3000/',
    SERVER_URL: 'http://localhost:5000/',
    GOOGLE_API_KEY: 'google_api_key',

    DB_USER: 'postgres',
    DB_HOST: 'postgres',
    DB_NAME: 'mootube',
    DB_PASSWORD: 'password',
    DB_PORT: 5432,

    LAST_PAGE_TOKEN_FILE: '/app/last_page_token'  
};
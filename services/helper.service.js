'use strict';

const fs = require('fs');

const millisToSeconds = value => Math.floor(value / 1000);

const sleep = n => new Promise(r => setTimeout(r, n * 1000));

const checkFileExists = f => new Promise(r => fs.access(f, fs.constants.F_OK, e => r(!e)));

module.exports = {
    checkFileExists,
    millisToSeconds,
    sleep
};
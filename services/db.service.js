'use strict';

const q = require('q');
const Pool = require('pg').Pool;

const config = require('../config/config');

const pool = new Pool({
	user: config.DB_USER,
	host: config.DB_HOST,
	database: config.DB_NAME,
	password: config.DB_PASSWORD,
	port: config.DB_PORT,

	max: 20,
	idleTimeoutMillis: 30000
});

// TODO: Add rollback on failure

const getVideos = (limit = 0, offset = 0, key = '') => {
	const defer = q.defer();
	// TODO: Use tsquery to improve search
	const query = `
		SELECT *
		FROM videos
		WHERE
			title LIKE '%${key}%'
			OR description LIKE '%${key}%'
		ORDER BY published_at DESC
		LIMIT ${limit}
		OFFSET ${offset}
	`;

	pool.query(query, (error, results) => {
		if (error) {
			defer.reject(error);
		} else {
			defer.resolve(results.rows);
		}
	});

	return defer.promise;
}

const insertVideo = video => {
	const defer = q.defer();
	const {
		videoId,
		title,
		description,
		thumbnail,
		published_at: publishedAt
	} = video;

	const query = `
		INSERT INTO videos
			(video_id, title, description, thumbnail, published_at)
		VALUES 
			($1, $2, $3, $4, $5)
	`;
	const params = [videoId, title, description, thumbnail, publishedAt];

	pool.query(query, params, (error, results) => {
		if (error) {
			console.log('Error occured while creating video');
			console.log(error);

			defer.reject(error);
		} else {
			console.log(`Inserted videoId: ${videoId}`);

			defer.resolve(results.insertId);
		}
	});

	return defer.promise;
}

module.exports = {
	getVideos,
	insertVideo
};
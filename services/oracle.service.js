'use strict';

const axios = require("axios");
const fs = require('fs');
const fsPromises = fs.promises;

const db = require('./db.service');
const helper = require('./helper.service');
const config = require('../config/config');

const _searchYoutube = async (query, pageToken = "") => {
    const part = 'snippet',
        key = config.GOOGLE_API_KEY,
        type = 'video';

    const url = `https://www.googleapis.com/youtube/v3/search?part=${part}&key=${key}&type=${type}&q=${query}&pageToken=${pageToken}&order=date`; // TODO: Add published after
    console.log(url);
    const response = await axios.get(url);
    return response.data;
}

const _storeLastPage = async pageToken => {
    let filehandle = null

    try {
        filehandle = await fsPromises.open(config.LAST_PAGE_TOKEN_FILE, 'w+');
        await filehandle.writeFile(pageToken);
    } finally {
        if (filehandle) {
            await filehandle.close();
        }
    }
}

const _retrieveLastPage = async () => {
    let filehandle = null;

    try {
        if (!await helper.checkFileExists(config.LAST_PAGE_TOKEN_FILE)) {
            return;
        }

        filehandle = await fsPromises.open(config.LAST_PAGE_TOKEN_FILE, 'r+');
        return await filehandle.readFile("utf8");
    } finally {
        if (filehandle) {
            return await filehandle.close();
        }
    }
}

const syncVideos = async () => {
    let pageToken = await _retrieveLastPage();
    do {
        console.log({ pageToken });
        const data = await _searchYoutube("football", pageToken);
        if (typeof data !== "undefined") {
            for (let item of data.items) {
                console.log({ [pageToken]: item.id.videoId });
                await db.insertVideo({
                    videoId: item.id.videoId,
                    title: item.snippet.title,
                    description: item.snippet.description,
                    thumbnail: item.snippet.thumbnails.default.url,
                    published_at: item.snippet.publishedAt
                });
            }

            pageToken = data.nextPageToken;
            await _storeLastPage(pageToken);
        } else {
            break;
        }

        await helper.sleep(20);
    } while (true);
}

module.exports = {
    syncVideos
};